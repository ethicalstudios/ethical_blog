<?php
/**
 * @file
 * ethical_blog.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function ethical_blog_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_blog_footer';
  $mini->category = '';
  $mini->admin_title = 'Blog footer';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b901c8af-460a-432e-847e-c32c228b5d7e';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-264654c8-342c-4d55-a997-cf3f5dbe8207';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'disqus-disqus_comments';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '264654c8-342c-4d55-a997-cf3f5dbe8207';
  $display->content['new-264654c8-342c-4d55-a997-cf3f5dbe8207'] = $pane;
  $display->panels['contentmain'][0] = 'new-264654c8-342c-4d55-a997-cf3f5dbe8207';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-264654c8-342c-4d55-a997-cf3f5dbe8207';
  $mini->display = $display;
  $export['oe_blog_footer'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_blog_main_content';
  $mini->category = '';
  $mini->admin_title = 'Blog main content';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b445bcae-7409-4dd8-a6e5-260a70529d87';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ad7876ed-c86a-432e-be6a-2592da10bd30';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'medium',
      'image_link' => '',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ad7876ed-c86a-432e-be6a-2592da10bd30';
  $display->content['new-ad7876ed-c86a-432e-be6a-2592da10bd30'] = $pane;
  $display->panels['contentmain'][0] = 'new-ad7876ed-c86a-432e-be6a-2592da10bd30';
  $pane = new stdClass();
  $pane->pid = 'new-184588f2-ca81-49e6-b0cf-b72b087edc3a';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '184588f2-ca81-49e6-b0cf-b72b087edc3a';
  $display->content['new-184588f2-ca81-49e6-b0cf-b72b087edc3a'] = $pane;
  $display->panels['contentmain'][1] = 'new-184588f2-ca81-49e6-b0cf-b72b087edc3a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-ad7876ed-c86a-432e-be6a-2592da10bd30';
  $mini->display = $display;
  $export['oe_blog_main_content'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_blog_meta_data';
  $mini->category = '';
  $mini->admin_title = 'Blog meta data';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '08901684-075e-4132-ab46-76b54ac1b3f0';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-2b3dc02b-bc10-4b83-ba7d-bf59a9424cd0';
  $pane->panel = 'contentmain';
  $pane->type = 'node_author';
  $pane->subtype = 'node_author';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '2b3dc02b-bc10-4b83-ba7d-bf59a9424cd0';
  $display->content['new-2b3dc02b-bc10-4b83-ba7d-bf59a9424cd0'] = $pane;
  $display->panels['contentmain'][0] = 'new-2b3dc02b-bc10-4b83-ba7d-bf59a9424cd0';
  $pane = new stdClass();
  $pane->pid = 'new-f359e4ba-4ecb-41de-9a6f-e0a311c42d4d';
  $pane->panel = 'contentmain';
  $pane->type = 'node_created';
  $pane->subtype = 'node_created';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'format' => 'panopoly_day',
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'f359e4ba-4ecb-41de-9a6f-e0a311c42d4d';
  $display->content['new-f359e4ba-4ecb-41de-9a6f-e0a311c42d4d'] = $pane;
  $display->panels['contentmain'][1] = 'new-f359e4ba-4ecb-41de-9a6f-e0a311c42d4d';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-2b3dc02b-bc10-4b83-ba7d-bf59a9424cd0';
  $mini->display = $display;
  $export['oe_blog_meta_data'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_blog_sidebar';
  $mini->category = '';
  $mini->admin_title = 'Blog sidebar';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '327ec5ab-927e-44fa-97bc-2eb629316a85';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['oe_blog_sidebar'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'oe_blog_tags';
  $mini->category = '';
  $mini->admin_title = 'Blog tags';
  $mini->admin_description = '';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'See more by';
  $display->uuid = '4780d017-8511-46fc-87fa-9ef4572dd378';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e7cea535-e900-4002-b96b-e8e1d90ef0ff';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_project';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_blog_project',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e7cea535-e900-4002-b96b-e8e1d90ef0ff';
  $display->content['new-e7cea535-e900-4002-b96b-e8e1d90ef0ff'] = $pane;
  $display->panels['contentmain'][0] = 'new-e7cea535-e900-4002-b96b-e8e1d90ef0ff';
  $pane = new stdClass();
  $pane->pid = 'new-523ea3ac-b01a-4446-9b5f-7fae4ac3370b';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_categories';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_blog_category',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '523ea3ac-b01a-4446-9b5f-7fae4ac3370b';
  $display->content['new-523ea3ac-b01a-4446-9b5f-7fae4ac3370b'] = $pane;
  $display->panels['contentmain'][1] = 'new-523ea3ac-b01a-4446-9b5f-7fae4ac3370b';
  $pane = new stdClass();
  $pane->pid = 'new-5506effc-a3ae-4603-a1ad-329e3f0168a1';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_country';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'ethical_blog_country',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '5506effc-a3ae-4603-a1ad-329e3f0168a1';
  $display->content['new-5506effc-a3ae-4603-a1ad-329e3f0168a1'] = $pane;
  $display->panels['contentmain'][2] = 'new-5506effc-a3ae-4603-a1ad-329e3f0168a1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['oe_blog_tags'] = $mini;

  return $export;
}
