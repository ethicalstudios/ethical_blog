<?php
/**
 * @file
 * ethical_blog.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ethical_blog_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_blog_post:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_blog_post';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'blog-post-page';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array(
    'panels_breadcrumbs_state' => 0,
    'panels_breadcrumbs_titles' => 'Blog',
    'panels_breadcrumbs_paths' => 'blog',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '70f442f3-a5b6-49ef-9d17-f95dec1cf5c5';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-87579035-0df8-4e1c-8d6b-584f9c029dbc';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_blog_meta_data';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '87579035-0df8-4e1c-8d6b-584f9c029dbc';
  $display->content['new-87579035-0df8-4e1c-8d6b-584f9c029dbc'] = $pane;
  $display->panels['contentmain'][0] = 'new-87579035-0df8-4e1c-8d6b-584f9c029dbc';
  $pane = new stdClass();
  $pane->pid = 'new-4c191b16-0369-4e88-881c-768b86fab18c';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_blog_main_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '4c191b16-0369-4e88-881c-768b86fab18c';
  $display->content['new-4c191b16-0369-4e88-881c-768b86fab18c'] = $pane;
  $display->panels['contentmain'][1] = 'new-4c191b16-0369-4e88-881c-768b86fab18c';
  $pane = new stdClass();
  $pane->pid = 'new-52f8e57f-3d41-41e2-993c-6297d361716e';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'node:easy_social_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array(
    'type' => 'none',
    'regions' => array(
      'contentmain' => 'contentmain',
      'footer' => 'footer',
    ),
  );
  $pane->uuid = '52f8e57f-3d41-41e2-993c-6297d361716e';
  $display->content['new-52f8e57f-3d41-41e2-993c-6297d361716e'] = $pane;
  $display->panels['contentmain'][2] = 'new-52f8e57f-3d41-41e2-993c-6297d361716e';
  $pane = new stdClass();
  $pane->pid = 'new-0d743eba-da65-4449-80a6-17c9039b6520';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_blog_tags';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '0d743eba-da65-4449-80a6-17c9039b6520';
  $display->content['new-0d743eba-da65-4449-80a6-17c9039b6520'] = $pane;
  $display->panels['contentmain'][3] = 'new-0d743eba-da65-4449-80a6-17c9039b6520';
  $pane = new stdClass();
  $pane->pid = 'new-5ed6cf48-64a9-4e5e-850c-f5785a1fad42';
  $pane->panel = 'footer';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_blog_footer';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5ed6cf48-64a9-4e5e-850c-f5785a1fad42';
  $display->content['new-5ed6cf48-64a9-4e5e-850c-f5785a1fad42'] = $pane;
  $display->panels['footer'][0] = 'new-5ed6cf48-64a9-4e5e-850c-f5785a1fad42';
  $pane = new stdClass();
  $pane->pid = 'new-f5991b4c-c1c2-4ec4-ad45-8a47b2f95a29';
  $pane->panel = 'sidebar';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_blog_sidebar';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f5991b4c-c1c2-4ec4-ad45-8a47b2f95a29';
  $display->content['new-f5991b4c-c1c2-4ec4-ad45-8a47b2f95a29'] = $pane;
  $display->panels['sidebar'][0] = 'new-f5991b4c-c1c2-4ec4-ad45-8a47b2f95a29';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_blog_post:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_blog_post:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_blog_post';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = 'blog-post-default';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_boxton';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '0ebbc47c-3935-44b8-932e-af305e9382b3';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e3d67ae0-7c7c-41d3-91b1-956307f7f727';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e3d67ae0-7c7c-41d3-91b1-956307f7f727';
  $display->content['new-e3d67ae0-7c7c-41d3-91b1-956307f7f727'] = $pane;
  $display->panels['contentmain'][0] = 'new-e3d67ae0-7c7c-41d3-91b1-956307f7f727';
  $pane = new stdClass();
  $pane->pid = 'new-b28f9a57-94ca-446c-a3f6-40e0a82017f8';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b28f9a57-94ca-446c-a3f6-40e0a82017f8';
  $display->content['new-b28f9a57-94ca-446c-a3f6-40e0a82017f8'] = $pane;
  $display->panels['contentmain'][1] = 'new-b28f9a57-94ca-446c-a3f6-40e0a82017f8';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_blog_post:default:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_blog_post:default:featured';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_blog_post';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'featured';
  $panelizer->css_class = 'blog-post-featured';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'f688936f-f4b8-4be1-b523-f1e3bd2b0dc0';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1c3c2668-771a-458b-b3e2-3fe792cd3357';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1c3c2668-771a-458b-b3e2-3fe792cd3357';
  $display->content['new-1c3c2668-771a-458b-b3e2-3fe792cd3357'] = $pane;
  $display->panels['contentmain'][0] = 'new-1c3c2668-771a-458b-b3e2-3fe792cd3357';
  $pane = new stdClass();
  $pane->pid = 'new-04a1711c-facc-45fd-aaff-cb702f785d7e';
  $pane->panel = 'contentmain';
  $pane->type = 'node_author';
  $pane->subtype = 'node_author';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '04a1711c-facc-45fd-aaff-cb702f785d7e';
  $display->content['new-04a1711c-facc-45fd-aaff-cb702f785d7e'] = $pane;
  $display->panels['contentmain'][1] = 'new-04a1711c-facc-45fd-aaff-cb702f785d7e';
  $pane = new stdClass();
  $pane->pid = 'new-2fb7cae0-454f-4668-b624-41081f7fc785';
  $pane->panel = 'contentmain';
  $pane->type = 'node_created';
  $pane->subtype = 'node_created';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'format' => 'panopoly_day',
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2fb7cae0-454f-4668-b624-41081f7fc785';
  $display->content['new-2fb7cae0-454f-4668-b624-41081f7fc785'] = $pane;
  $display->panels['contentmain'][2] = 'new-2fb7cae0-454f-4668-b624-41081f7fc785';
  $pane = new stdClass();
  $pane->pid = 'new-8355ce15-5562-43a9-9df8-081c61396c90';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '30',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'full',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '8355ce15-5562-43a9-9df8-081c61396c90';
  $display->content['new-8355ce15-5562-43a9-9df8-081c61396c90'] = $pane;
  $display->panels['contentmain'][3] = 'new-8355ce15-5562-43a9-9df8-081c61396c90';
  $pane = new stdClass();
  $pane->pid = 'new-30f58c58-43a4-4d48-96a3-c6fc24b50bba';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => 1,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '30f58c58-43a4-4d48-96a3-c6fc24b50bba';
  $display->content['new-30f58c58-43a4-4d48-96a3-c6fc24b50bba'] = $pane;
  $display->panels['contentmain'][4] = 'new-30f58c58-43a4-4d48-96a3-c6fc24b50bba';
  $pane = new stdClass();
  $pane->pid = 'new-f9ed8997-b4b9-42f0-a97a-73dfa04ab5c8';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'medium',
      'image_link' => 'content',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f9ed8997-b4b9-42f0-a97a-73dfa04ab5c8';
  $display->content['new-f9ed8997-b4b9-42f0-a97a-73dfa04ab5c8'] = $pane;
  $display->panels['sidebar'][0] = 'new-f9ed8997-b4b9-42f0-a97a-73dfa04ab5c8';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_blog_post:default:featured'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_blog_post:default:teaser';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_blog_post';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'teaser';
  $panelizer->css_class = 'blog-post-teaser';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '8eb73ff1-5b9e-4e3e-bd01-163b89e716b1';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-6d81afeb-970e-43e9-b3d3-0f674d592770';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '30',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'trim',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6d81afeb-970e-43e9-b3d3-0f674d592770';
  $display->content['new-6d81afeb-970e-43e9-b3d3-0f674d592770'] = $pane;
  $display->panels['contentmain'][0] = 'new-6d81afeb-970e-43e9-b3d3-0f674d592770';
  $pane = new stdClass();
  $pane->pid = 'new-0400c5d4-707a-4695-aa7b-4825c617cbad';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0400c5d4-707a-4695-aa7b-4825c617cbad';
  $display->content['new-0400c5d4-707a-4695-aa7b-4825c617cbad'] = $pane;
  $display->panels['contentmain'][1] = 'new-0400c5d4-707a-4695-aa7b-4825c617cbad';
  $pane = new stdClass();
  $pane->pid = 'new-e54b5ca6-e046-4faa-b7cb-8a66f55c9a7c';
  $pane->panel = 'header';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e54b5ca6-e046-4faa-b7cb-8a66f55c9a7c';
  $display->content['new-e54b5ca6-e046-4faa-b7cb-8a66f55c9a7c'] = $pane;
  $display->panels['header'][0] = 'new-e54b5ca6-e046-4faa-b7cb-8a66f55c9a7c';
  $pane = new stdClass();
  $pane->pid = 'new-09db5995-88b4-422c-8398-fab2f0a4f4c2';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'thumbnail',
      'image_link' => 'content',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '09db5995-88b4-422c-8398-fab2f0a4f4c2';
  $display->content['new-09db5995-88b4-422c-8398-fab2f0a4f4c2'] = $pane;
  $display->panels['sidebar'][0] = 'new-09db5995-88b4-422c-8398-fab2f0a4f4c2';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_blog_post:default:teaser'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_blog_post:default:tiny';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_blog_post';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'tiny';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'bd1d8509-0e29-463d-9b24-b137935fdf21';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c63b5b27-ddb8-423a-9493-176de420d868';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'none',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c63b5b27-ddb8-423a-9493-176de420d868';
  $display->content['new-c63b5b27-ddb8-423a-9493-176de420d868'] = $pane;
  $display->panels['contentmain'][0] = 'new-c63b5b27-ddb8-423a-9493-176de420d868';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_blog_post:default:tiny'] = $panelizer;

  return $export;
}
