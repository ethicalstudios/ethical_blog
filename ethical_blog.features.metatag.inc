<?php
/**
 * @file
 * ethical_blog.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ethical_blog_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:oe_blog_post.
  $config['node:oe_blog_post'] = array(
    'instance' => 'node:oe_blog_post',
    'config' => array(
      'image_src' => array(
        'value' => '[node:field_featured_image]',
      ),
      'og:type' => array(
        'value' => 'blog',
      ),
      'og:image' => array(
        'value' => '[node:field_featured_image]',
      ),
    ),
  );

  return $config;
}
