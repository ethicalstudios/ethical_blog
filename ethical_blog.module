<?php
/**
 * @file
 * Code for the Ethical blog feature.
 */

include_once 'ethical_blog.features.inc';

//adjust the node edit form
function ethical_blog_form_oe_blog_post_node_form_alter(&$form, $form_state, $form_id){
  ethical_admin_adjust_node_edit_form($form);
}

/**
 * Implements hook_field_formatter_info().
 * Adds field formatters applied, for example, via panelizer to the full page override of a blog post
 * /admin/structure/types/manage/oe_blog_post/panelizer/page_manager/content
 */
function ethical_blog_field_formatter_info() {
  return array(
    'ethical_blog_category' => array(
      'label' => t('Blog category'),
      'field types' => array('taxonomy_term_reference')
    ),
    'ethical_blog_country' => array(
      'label' => t('Blog country'),
      'field types' => array('country')
    ),
    'ethical_blog_project' => array(
      'label' => t('Blog project'),
      'field types' => array('entityreference')
    )
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 */
function ethical_blog_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $lang = '';

  if(module_exists('ethical_translation')){
    $lang = ethical_translation_get_lang_url_prefix();
  }

  $labels = ethical_taxonomy_get_search_api_labels();

  $element = array();
  switch ($display['type']) {
    case 'ethical_blog_category':
      $element = array_merge($element, ethical_taxonomy_get_category_markup('blog', $items, $labels, $lang));
    break;
    case 'ethical_blog_country':
      $element = array_merge($element, ethical_taxonomy_get_country_markup('blog', $items, $labels, $lang));
    break;
    case 'ethical_blog_project':
      $element = array_merge($element, ethical_taxonomy_get_project_markup('blog', $items, $labels, $lang));
    break;
  }
  return $element;
}

/**
 * Implements hook_field_formatter_prepare_view().
 *
 * This preloads all taxonomy terms for multiple loaded objects at once and
 * unsets values for invalid terms that do not exist.
 */
function ethical_blog_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $display) {
  foreach ($entities as $id => $entity) {
    if($display[$id]['type'] == 'ethical_blog_category'){
      taxonomy_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, $items, $display);
    }
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function ethical_blog_module_implements_alter(&$implementations, $hook) {
  // Move our hooks to the end so they are executed last.
  if ($hook == 'modules_enabled') {
    $group = $implementations['ethical_blog'];
    unset($implementations['ethical_blog']);
    $implementations['ethical_blog'] = $group;
  }
}

/**
 * Implements hook_modules_enabled().
 *
 * This hook is implemented to assign some default permissions for Blog Posts. This has to be done in this hook to run after
 * both features and defaultconfig which power the functionality. Hopefully a
 * more general solution can be found.
 * @see http://drupal.org/node/1837312
 *
 *
 * copied from panopoly_pages.module
 *
 */
function ethical_blog_modules_enabled($modules) {

  // Only run this logic if we are executing as part of an install profile
  // And only for this particular module.
  if (drupal_installation_attempted() && in_array('ethical_blog', $modules)) {

    // Rebuild some caches so this all works right.
    features_revert_module('ethical_blog');
    drupal_static_reset('panelizer_entity_plugin_get_handler');

    // Define some permissions for the editor role.
    $editor = user_role_load_by_name('editor');
    user_role_grant_permissions($editor->rid, array('administer panelizer node oe_blog_post content'));

    // Define some permissions for the administrator role.
    $administrator = user_role_load_by_name('administrator');
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post breadcrumbs'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post content'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post context'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post defaults'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post layout'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post overview'));
    user_role_grant_permissions($administrator->rid, array('administer panelizer node oe_blog_post settings'));
  }
}
